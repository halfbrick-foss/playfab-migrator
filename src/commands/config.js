const {flags} = require('@oclif/command')
const {cli} = require('cli-ux')
const fs = require('fs').promises
const os = require('os')
const AuthenticatedCommand = require('../authenticated-command')

class ConfigCommand extends AuthenticatedCommand {
  async run() {
    this.configFilePath = `${os.homedir()}/.playfab/config`
    const {args: {action}, flags} = this.parse(ConfigCommand)
    switch (action) {
    case 'create':
      await this.createConfig(flags)
      break
    case 'show':
      await this.showConfig()
      break
    case 'purge':
      await this.purgeConfig()
      break
    default:
      this.error('Invlaid Action passed to this command')
    }
  }

  /**
   * Parses the flags inputs and checks if we need to set anything by promting the user
   * @param {*} flags
   */
  async ensureFlags(flags) {
    let {user = false, password = false} = flags
    if (!user) {
      user = await cli.prompt('What is your Playfab username?')
    }
    this.user = user

    if (!password) {
      password = await cli.prompt('What is your Playfab Password?', {type: 'hide'})
    }
    this.password = password
  }

  /**
   * createConfig
   *
   * creates the config files necessary for the CLI to operate
   */
  async createConfig(flags) {
    let overwrite = true
    const shouldConfirm = this.hasConfig()
    if (shouldConfirm) {
      overwrite = await cli.confirm('this CLI is already configured.\nare you sure you wish to overwrite the current config? (this cannot be undone!)')
    }
    if (overwrite === false) {
      this.exit()
    }
    await this.ensureFlags(flags)
    cli.action.start('Creating Config')
    await this.writeConfigToFile()
    cli.action.stop()
    this.log('Configuration Complete, Happy Migrating!')
  }

  /**
   * Does the Actual Creation of the Config File
   */
  async writeConfigToFile() {
    // replace the entire contents of the file with the new config string
    var data = `ADMIN_EMAIL=${this.user}\nADMIN_PASSWORD=${this.password}`
    await fs.writeFile(this.configFilePath, data)
  }

  /**
   * showConfig
   *
   * prints the configuration details in the terminal
   */
  async showConfig() {
    if (this.hasConfig()) {
      const data = await fs.readFile(this.configFilePath)
      this.log(data.toString())
    } else {
      this.error('No config found on your system, please configure the CLI using the config command')
    }
  }

  /**
   * purgeConfig
   *
   * this clears the config info from the system
   */
  async purgeConfig() {
    if (this.hasConfig()) {
      await fs.unlink(this.configFilePath)
      this.log('Config Purged!')
    } else {
      this.error('No config found on your system, please configure the CLI using the config command')
    }
  }
}

ConfigCommand.args = [
  {
    name: 'action', // name of arg to show in help and reference with args[name]
    required: true, // make the arg required with `required: true`
    description: 'Action to perform on the CLI\'s config', // help description
    default: 'create', // default value if no arg input
    options: ['create', 'show', 'purge'], // only allow input to be from a discrete set
  },
]

ConfigCommand.flags = {
  user: flags.string({char: 'u', description: 'the username of the account to authenticate the CLI against'}),
  password: flags.string({char: 'p', description: 'the password of the account to authenticate the CLI against'}),
}

ConfigCommand.description = `Configure the Dashboard CLI
use this command to ensure your Playfab session is authenticated for your playfab account
`

module.exports = ConfigCommand
