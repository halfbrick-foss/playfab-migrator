const {flags} = require('@oclif/command')
const AuthenticatedCommand = require('../authenticated-command')
const fetch = require('node-fetch')
const {cli} = require('cli-ux')
const Listr = require('listr')

class SegmentsCommand extends AuthenticatedCommand {
  async run() {
    this.log('SEGMENTS MIGRATOR\n')
    const {flags, args} = this.parse(SegmentsCommand)
    const {action} = args
    const {origin, destination} = flags

    if (action === 'migrate') {
      await this.migrate(origin, destination)
    }
  }

  /**
   * Loads Title Data for the Studio
   */
  async loadTitleData() {
    cli.action.start('Gathering Title Data')
    const request = await fetch('https://developer.playfab.com/en-US/my-games/all', {
      headers: {
        cookie: this.authCookie,
      },
      method: 'GET',
    })
    const {Titles} = await request.json()
    this.AvailableTitles = Titles
    cli.action.stop()
  }

  /**
   * Traverse the Loaded Available titles and Print the Job Summary
   *
   * @param {*} originTitleID
   * @param {*} destinationTitleID
   */
  getMigrationSummary(originTitleID, destinationTitleID) {
    const {Name: origin} = this.AvailableTitles.find(({TitleId}) => TitleId === originTitleID)
    const {Name: destination} = this.AvailableTitles.find(({TitleId}) => TitleId === destinationTitleID)
    return `\nJOB SUMMARY:\nMigrating All Player Segments from '${origin}' to '${destination}' \n`
  }

  /**
   * Runs a Segment Migration from the Origin to the Destination Title
   *
   * @param {*} originTitleID
   * @param {*} destinationTitleID
   */
  async migrate(originTitleID, destinationTitleID) {
    cli.action.start('Checking Config')
    this.loadConfig()
    cli.action.stop()
    // login to playfab using the headless browser
    cli.action.start('Creating Playfab Session')
    await this.initSession()
    cli.action.stop()
    await this.loadTitleData()
    this.log(this.getMigrationSummary(originTitleID, destinationTitleID))
    const doMigration =  await cli.confirm('Are you sure you wish to progress? (y/n) This cannot be undone!')
    this.log('')
    if (doMigration) {
      const migrationTasks = new Listr([
        {
          title: 'Purge Segments from Destination Title',
          task: () => this.purgeSegments(destinationTitleID),
        },
        {
          title: 'Get Segments From Origin Title',
          task: async ctx => {
            ctx.originSegments = await this.listSegments(originTitleID)
          },
        },
        {
          title: 'Load Segments From Origin into Destination title',
          task: async ctx => {
            await ctx.originSegments.forEach(async ({SegmentId}) => {
              const segment = await this.getSegment(originTitleID, SegmentId)
              await this.addSegment(destinationTitleID, segment)
            })
          },
        },
      ])
      // run the job and handle the errors
      await migrationTasks.run()
      await this.closeSession()
      this.log('\nMigration Complete!')
    }
  }

  /**
   * Returns a List of Segments for the given title
   *
   * @param {*} titleID
   */
  async listSegments(titleID) {
    const request = await fetch(`https://developer.playfab.com/api/en-US/${titleID}/segments`, {
      headers: {
        cookie: this.authCookie,
      },
      method: 'GET',
    })
    const data = await request.json()
    return data
  }

  /**
   * Returns Segment Specific Details
   *
   * @param {*} titleID
   * @param {*} segmentID
   */
  async getSegment(titleID, segmentID) {
    const request = await fetch(`https://developer.playfab.com/api/en-US/${titleID}/segments/${segmentID}/edit`, {
      headers: {
        cookie: this.authCookie,
      },
      method: 'GET',
    })
    const {Data} = await request.json()
    return Data
  }

  /**
   * Adds a Segment to a Title
   *
   * @param {*} titleID
   * @param {*} segmentData
   */
  async addSegment(titleID, segmentData) {
    segmentData.SegmentId = null
    await fetch(`https://developer.playfab.com/api/en-US/${titleID}/segments/new`, {
      headers: {
        cookie: this.authCookie,
        'content-type': 'application/json',
      },
      body: JSON.stringify(segmentData),
      method: 'POST',
    })
  }

  /**
   * Removes All Segments from a Title
   *
   * @param {*} titleID
   */
  async purgeSegments(titleID) {
    const segments = await this.listSegments(titleID)
    const segmentIds = segments.map(({SegmentId}) => {
      return SegmentId
    })
    if (segmentIds.length > 0) {
      await this.deleteSegments(titleID, segmentIds)
    }
  }

  /**
   * Deletes Specific Segments from a Title
   *
   * @param {*} titleID
   * @param {*} segmentIDs
   */
  async deleteSegments(titleID, segmentIDs) {
    await fetch(`https://developer.playfab.com/api/en-US/${titleID}/segments`, {
      headers: {
        cookie: this.authCookie,
        'content-type': 'application/json',
      },
      body: JSON.stringify({ids: segmentIDs}),
      method: 'DELETE',
    })
  }
}

SegmentsCommand.args = [
  {
    name: 'action', // name of arg to show in help and reference with args[name]
    required: true, // make the arg required with `required: true`
    description: 'Action to perform on a title\'s segments', // help description
    default: 'migrate', // default value if no arg input
    options: ['migrate'], // only allow input to be from a discrete set
  },
]

SegmentsCommand.description = `SEGMENTS MIGRATOR
This tool allows you to manmage playfab segments in ways that are not covered by the Admin API
`

SegmentsCommand.flags = {
  origin: flags.string({char: 'o', description: 'Playfab Title ID where the segments originate', required: true}),
  destination: flags.string({char: 'd', description: 'Playfab Title ID where the segments should go', required: true}),
}

module.exports = SegmentsCommand
