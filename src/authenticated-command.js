const {Command} = require('@oclif/command')
const puppeteer = require('puppeteer')
const fs = require('fs')

class AuthenticatedCommand extends Command {
  hasConfig() {
    const homedir = require('os').homedir()
    return fs.existsSync(`${homedir}/.playfab/config`)
  }

  loadConfig() {
    if (this.hasConfig()) {
      const homedir = require('os').homedir()
      require('dotenv').config({path: `${homedir}/.playfab/config`})
    } else {
      this.error('No config found on your system, please configure the CLI using the config command')
    }
  }

  async initSession() {
    const browser = await puppeteer.launch()
    const page = await browser.newPage()

    // login to the Application
    await this.login(page)
    // multiple returns because JS
    this.session =  {browser, page}
    this.authCookie = await this.parseSessionCookies()
  }

  async closeSession() {
    await this.session.browser.close()
  }

  async parseSessionCookies() {
    let cookieString = ''
    const cookies = await this.session.page.cookies()
    if (cookies.length === 0) {
      this.error('Invalid config, please configure the CLI using the config command')
    }
    cookies.forEach(({name, value}) => {
      cookieString += `${name}=${value}; `
    })
    return cookieString
  }

  async login(page) {
    await page.goto('https://developer.playfab.com/en-US/login')

    await page.setViewport({width: 2560, height: 1306})

    await page.waitForSelector('#body-help-content > #login #Data_UserEmail')
    await page.waitForSelector('#body-help-content > #login #Data_Password')
    await page.waitForSelector('#login > .confirm > ul > li > .button')
    await page.type('#Data_UserEmail', process.env.ADMIN_EMAIL)
    await page.type('#Data_Password', process.env.ADMIN_PASSWORD)
    await page.waitForResponse(
      'https://developer.playfab.com/en-US/check-email'
    )
    const button = await page.$('button[type="submit"]')
    await button.click()

    // wait for the page to do its double refresh
    await page.waitForNavigation()
    // await page.waitForNavigation()

    await page.waitForSelector('.studios-and-titles')
    await this.delay(2000)
  }

  delay(time) {
    return new Promise(function (resolve) {
      setTimeout(resolve, time)
    })
  }
}

module.exports = AuthenticatedCommand
