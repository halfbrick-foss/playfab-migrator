@halfbrick/playfab-migrator
===========================

CLI that performs tasks usually done manually on the playfab dashboard

<!-- toc -->
* [Installation](#installation)
* [Configuration](#configuration)
* [Commands](#commands)
<!-- tocstop -->
# Installation
<!-- installation -->
## NPM
```sh-session
npm install -g @halfbrick/playfab-migrator
```
<!-- installationstop -->
# Configuration
<!-- configuration -->
IMPORTANT: This tool does not currently support accounts that login with Microsoft.

In order to use this CLI you'll need to have a playfab account.

run the following command to configure the CLI:
```bash
playfab-migrator config create
```

this will prompt you for your playfab email address and password, once complete your credentials are saved for future use by the CLI in the following file `~/.playfab/config`. you can manage your config through the config command as listed below

<!-- configurationstop -->
# Commands
<!-- commands -->
* [`playfab-migrator config ACTION`](#playfab-migrator-config-action)
* [`playfab-migrator help [COMMAND]`](#playfab-migrator-help-command)
* [`playfab-migrator segments ACTION`](#playfab-migrator-segments-action)

## `playfab-migrator config [ACTION]`

Configure the Dashboard CLI

```
USAGE
  $ playfab-migrator config ACTION

ARGUMENTS
  ACTION  (create|show|purge) [default: create] Action to perform on the CLI's config

OPTIONS
  -p, --password=password  the password of the account to authenticate the CLI against
  -u, --user=user          the username of the account to authenticate the CLI against

DESCRIPTION
  use this command to ensure your Playfab session is authenticated
```

## `playfab-migrator help [COMMAND]`

display help for playfab-migrator

```
USAGE
  $ playfab-migrator help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

## `playfab-migrator segments ACTION`

SEGMENTS MIGRATOR

```
USAGE
  $ playfab-migrator segments ACTION

ARGUMENTS
  ACTION  (migrate) [default: migrate] Action to perform on a title's segments

OPTIONS
  -d, --destination=destination  (required) Playfab Title ID where the segments should go
  -o, --origin=origin            (required) Playfab Title ID where the segments originate

DESCRIPTION
  This tool allows you to manmage playfab segments in ways that are not covered by the Admin API
```
<!-- commandsstop -->
